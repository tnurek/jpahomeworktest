CREATE USER dragons WITH PASSWORD 'dragons';

CREATE  DATABASE dragons OWNER dragons;

CREATE TABLE dragons (
  id SERIAL PRIMARY KEY,
  name VARCHAR(256),
  wingspan INT
);

CREATE TABLE eggs (
  id SERIAL PRIMARY KEY,
  weight INT,
  color VARCHAR(256),
  dragon INT,
  FOREIGN KEY (dragon) REFERENCES dragons (id)
);
