package pl.codementors.jpahomework.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "dragons")
public class Dragon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // !
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "wingspan")
    private int wingspan;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dragon", fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Egg> eggs = new ArrayList<>();

    public Dragon(String name, int wingspan, List<Egg> eggs) {
        this.name = name;
        this.wingspan = wingspan;
        this.eggs = eggs;
    }

    public Dragon(String name, int wingspan) {
        this.name = name;
        this.wingspan = wingspan;
    }

    public Dragon() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWingspan() {
        return wingspan;
    }

    public void setWingspan(int wingspan) {
        this.wingspan = wingspan;
    }

    public List<Egg> getEggs() {
        return eggs;
    }

    public void setEggs(List<Egg> eggs) {
        this.eggs = eggs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Dragon{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", wingspan=" + wingspan +
                ", eggs=" + eggs +
                '}';
    }
}
