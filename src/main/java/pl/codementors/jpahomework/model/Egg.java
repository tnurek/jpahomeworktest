package pl.codementors.jpahomework.model;

import javax.persistence.*;

@Entity
@Table(name = "eggs")
public class Egg {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //!
    private int id;

    @Column
    private int weight;

    @Column
    private String color;

    @ManyToOne
    @JoinColumn(name = "dragon", referencedColumnName = "id")
    private Dragon dragon;

    public Egg(int weight, String color, Dragon dragon) {
        this.weight = weight;
        this.color = color;
        this.dragon = dragon;
    }

    public Egg() {
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Dragon getDragon() {
        return dragon;
    }

    public void setDragon(Dragon dragon) {
        this.dragon = dragon;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Egg{" +
                "id=" + id +
                ", weight=" + weight +
                ", color='" + color + '\'' +
                ", dragon=" + dragon +
                '}';
    }

    public void setId(int id) {
        this.id = id;
    }
}
