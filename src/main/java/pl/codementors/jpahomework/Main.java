package pl.codementors.jpahomework;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.codementors.jpahomework.model.Dragon;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;


public class Main extends Application {

    public static void main(String[] args){ launch(args);}

    public void start(Stage stage) throws Exception{
        URL fxml = Main.class.getResource("/pl/codementors/view/dragons_list.fxml");
        ResourceBundle rb = ResourceBundle.getBundle("pl.codementors.view.messages.dragons_list_msg");

        FXMLLoader loader = new FXMLLoader(fxml, rb);
        Parent root = loader.load();

        Scene scene = new Scene(root, 800,600);

        stage.setTitle(rb.getString("applicationTitle"));
        stage.setScene(scene);

        stage.setOnCloseRequest(event -> DragonsEntityManagerFactory.close());

        stage.show();
    }


}
