package pl.codementors.jpahomework;

import pl.codementors.jpahomework.model.Dragon;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public class DragonsDao {

    public Dragon persist(Dragon dragon){
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(dragon);
        tx.commit();
        em.close();
        return dragon;
    }

    public Dragon merge(Dragon dragon){
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        dragon = em.merge(dragon);
        tx.commit();
        em.close();
        return dragon;
    }

    public List<Dragon> findAll(){
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        List<Dragon> dragons = em.createQuery("select d from Dragon d").getResultList();
        return dragons;
    }

}
