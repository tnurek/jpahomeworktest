package pl.codementors.jpahomework;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DragonsEntityManagerFactory {
    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("dragonsPU");

    public static final EntityManager  createEntityManager() {return emf.createEntityManager(); }

    public static void close() {emf.close();}
}
