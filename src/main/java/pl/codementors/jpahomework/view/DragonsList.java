package pl.codementors.jpahomework.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.IntegerStringConverter;
import pl.codementors.jpahomework.DragonsDao;
import pl.codementors.jpahomework.model.Dragon;
import pl.codementors.jpahomework.model.Egg;

import javax.persistence.Table;
import java.net.URL;
import java.util.ResourceBundle;

public class DragonsList implements Initializable {

    @FXML
    private TableView<Dragon> dragonsTable;

    @FXML
    private TableView<Egg> eggsTable;

    @FXML
    private TableColumn<Dragon, String> nameColumn;

    @FXML
    private TableColumn<Dragon, Integer> wingspanColumn;

    @FXML
    private TableColumn<Dragon, Integer> dragonIdColumn;

    @FXML
    private TableColumn<Egg, String> colorColumn;

    @FXML
    private TableColumn<Egg, Integer> weightColumn;

    private ObservableList<Dragon> dragons = FXCollections.observableArrayList();

    private ObservableList<Egg> eggs = FXCollections.observableArrayList();

    private ResourceBundle rb;

    private ObjectProperty<Dragon> selectedDragon = new SimpleObjectProperty<>(null);

    private DragonsDao dragonsDao;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle){

        rb = resourceBundle;

//        Dragon andrzej = new Dragon("Andrzej", 300);
//        dragons.add(andrzej);
//        dragonsDao.persist(andrzej);




        selectedDragon.addListener(new ChangeListener<Dragon>() {
            @Override
            public void changed(ObservableValue<? extends Dragon> observableValue, Dragon oldValue, Dragon newValue) {
                eggs.clear();
                if (newValue != null) {
                    eggs.addAll(newValue.getEggs());
                }
            }
        });

        dragonIdColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));


        nameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        nameColumn.setOnEditCommit(event -> {
            Dragon dragon = event.getRowValue();
            dragon.setName(event.getNewValue());
            dragonsDao.merge(dragon);
        });

        wingspanColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        wingspanColumn.setOnEditCommit(event -> {
            Dragon dragon = event.getRowValue();
            dragon.setWingspan(event.getNewValue());
            dragonsDao.merge(dragon);
        });

        dragonsTable.setItems(dragons);
        eggsTable.setItems(eggs);
        dragons.addAll(dragonsDao.findAll());

    }
}
